#include "texas_holdem.hpp"

using namespace std;
typedef unsigned long long gameStateId_t;

TexasHoldem::TexasHoldem(): nextTableId_(0), dbPath_("applications/poker/databases/storage.sqlite"){
	DBUtils &db = DBUtils::getInstance();
	db.openConnections("applications/poker/databases/storage.sqlite",10);
}
TexasHoldem::TexasHoldem(std::string dbPath): nextTableId_(0), dbPath_(dbPath){
	DBUtils &db = DBUtils::getInstance();
	db.openConnections(dbPath,10);
}

std::vector<int> TexasHoldem::getTableIds(){
	std::vector<int> keys;
	// Retrieve all keys
	boost::copy(tables_ | boost::adaptors::map_keys, std::back_inserter(keys));
	return keys;
}

bool TexasHoldem::createTable(int minPlayersNum,int maxPlayersNum){
	if (tables_.size()+1 > tables_.max_size())
		throw GameError("Too many tables",-1);
	TablePtr table(new Table(nextTableId_,minPlayersNum,maxPlayersNum));
	tables_.insert(std::make_pair(nextTableId_,table));
	++nextTableId_;
	return true;
}

bool TexasHoldem::deleteTable(int tableId){
	std::map<int,TablePtr>::iterator tabIt = tables_.find(tableId);
	if(tabIt == tables_.end())
		throw GameError("Table does not exist",-1);
	tables_.erase(tabIt);
	return true;
}

std::string TexasHoldem::getGameStatus(int playerId){
	if (!isLogged(playerId))
		throw GameError("Player is not logged in",playerId);
	int tabId = players_[playerId]->getTableId();
	if(tabId<0)
		throw GameError("Player is not assigned to any table",playerId);
	std::map<int,TablePtr>::iterator tabIt = tables_.find(tabId);
	if(tabIt == tables_.end())
		throw GameError("No such table",playerId);
	return (*tabIt).second->getTableStatus(playerId);
}
bool TexasHoldem::executePlayerAction(int playerId, int playerAction, unsigned long bid){
	TablePtr table = tables_[players_[playerId]->getTableId()];
	return table->executePlayerAction(playerId, playerAction,bid);
}

gameStateId_t TexasHoldem::getLastModified(int playerId){
	std::map<int,TablePtr>::iterator it = tables_.find(players_[playerId]->getTableId());
	if(it==tables_.end())
		throw GameError("No such table",playerId);
	return ((*it).second)->getLastModified();
}
/**
 * Zapisuje fakt wysłania przez użytkownika requestu
 * uwaga, nie używać markModified!
 * @param int player_id
 */
void TexasHoldem::markPlayerRequest(int playerId) {
	uniLock lock(mutex_);
	if (!isLogged(playerId))
		throw GameError("Player is not logged in (m)",playerId);
	std::map<int,PlayerPtr>::iterator it = players_.find(playerId);
	if(it == players_.end())
		throw GameError("Player is not logged in (m)",playerId);
	(*it).second->markRequest();
}
/*
 * wywołuje removeAllIdleFor dla wszystkich stołów
 * Można dodać parametr domyślny playerId=0 lub tableId=0 jeśli ma być dla konkretnego stołu
 */
void TexasHoldem::removeAllIdleFor(int seconds){
	for(std::map<int,TablePtr>::iterator it=tables_.begin(); it != tables_.end(); ++it)
	{
		(*it).second->removeAllIdleFor(seconds);
	}
}
/*
 * Tworzy nowy obiekt Player. Żeby dodać playera do stołu wołamy funkcję joinTable.
 */
bool TexasHoldem::addPlayer(int playerId){
	if( (players_.size() + 1) > players_.max_size() )
		throw GameError("Too many players in the game",playerId);
	if (isLogged(playerId))
		throw GameError("Player is already logged in",playerId);
	/* Get player's money from database */
	PlayerPtr p(new Player(playerId,getMoneyFromDB(playerId)));//tmp
	players_.insert(std::make_pair(playerId,p));
	return true;
}
/*
 * Dołącza playera do danego stołu.
 */
bool TexasHoldem::joinTable(int tableId, int playerId){
	if (!isLogged(playerId))
		throw GameError("Player is not logged in ",playerId);
	std::map<int,TablePtr>::iterator tabIt = tables_.find(tableId);
	if (tabIt == tables_.end())
		throw GameError("There is no shuch table",playerId);
	return (*tabIt).second->addPlayerToTable(players_[playerId]);
}
/*
 * Usuwa playera całkowicie z gry.
 */
bool TexasHoldem::removePlayer(int playerId){
	leaveTable(playerId);
	std::map<int,PlayerPtr>::iterator it = players_.find(playerId);
	players_.erase(it);
	return true;
}
/*
 * Odłącza gracza z danego stołu. Gracz pozostaje w obiekcie gry i może dołączyć do innego stołu.
 */
bool TexasHoldem::leaveTable(int playerId){
	std::map<int,PlayerPtr>::iterator it = players_.find(playerId);
	if (it == players_.end())
		throw GameError("Player is not logged in (th)",playerId);
	int tableId = ((*it).second)->getTableId();
	if(tableId<0)
		throw GameError("Player is not assigned to any table",playerId);
	std::map<int,TablePtr>::iterator tabIt = tables_.find(tableId);
	if (tabIt == tables_.end())
		throw GameError("There is no such table",playerId);
	return (*tabIt).second->removePlayerFromTable(playerId);
}

bool TexasHoldem::isLogged(int playerId){
	if (players_.empty())
		return false;
	std::map<int,PlayerPtr>::iterator it = players_.find(playerId);
	if (it == players_.end())
		return false; //gracza nie jest zalogowany
	return true;
}

int TexasHoldem::countAllPlayers(){
	return players_.size();
}

int TexasHoldem::countTablePlayers(int tableId){
	if(tables_.size() == 0)
		return 0;
	std::map<int, TablePtr>::iterator it = tables_.find(tableId);
	if(it == tables_.end())
		return 0;
	return ((*it).second)->countPlayers();
}

int TexasHoldem::getPlayerPosition(int playerId){
	if(!isLogged(playerId))
		throw GameError("Player is not logged in",playerId);
	int position = players_[playerId]->getTablePosition();
	if(position == -1)
		throw GameError("Player is not assigned to any table",playerId);
	return position;
}

double TexasHoldem::getMoneyFromDB(int playerId){
	using namespace soci;
	try{
		session sql(DBUtils::getInstance().getConnPool());
		indicator ind;
		double money;
		sql << "select money from auth_user where id = :playerId", into(money,ind), use(playerId);
		if (ind == i_ok)
			return money;
	}catch (std::exception const &e)
	{
		std::cerr << "Error: " << e.what() << '\n';
	}
	return 0;
}

