#include "player.hpp"

const std::string Player::figuresNames_[]  = {"empty", "high card", "pair", "two pairs", "three of a kind", "straight",
											"flush", "full house", "four of a kind", "straight flush", "royal flush"};

Player::Player(int player_id, double money):id_(player_id),
											  tableId_(-1),
											  isPlaying_(false),
											  handFigure_(-1),
											  tablePosition_(-1),
											  hasMoved_(false),
											  money_(money),
											  bidPerRound_(0)
{
	lastRequest_ = boost::posix_time::microsec_clock::universal_time();
}

bool Player::operator==(int player_id) {
	return id_ == player_id;
}

void Player::markRequest() {
	lastRequest_ = boost::posix_time::microsec_clock::universal_time();
}

bool Player::isIdleFor(boost::posix_time::time_duration &duration) {
	return boost::posix_time::microsec_clock::universal_time() - lastRequest_ > duration;
}

void Player::leaveTable(){
	reset();
	tablePosition_=-1;
}

void Player::reset(){
	isPlaying_ = true;
	playerCards_.clear();
	bestHand_.clear();
	handFigure_ = -1;
	resetRound();
}
bool Player::addMoneyToPot(int value){
	if(money_ < value){
		throw GameError ("Not enough money to bet");
	}
	else{
		money_-=value;
		bidPerRound_ +=value;
	}
	return true;
}
