#ifndef CARD_DECK_HPP_
#define CARD_DECK_HPP_
#include <algorithm>
#include <iostream>
#include <vector>
#include "card.hpp"

/**
 * A deck card class.
 * Represents deck of cards in poker game. Holds 52 playing cards and shuffles them
 * at the beginning of each poker hand.
 */
class CardDeck{
public:
	/**
	 * A constructor.
	 * After initializing of class members it executes shuffleCards() method
	 */
	CardDeck();
	/**
	 * A member method.
	 * Resets the deck (adds 52 cards to the deck) and shuffles all the cards in the deck
	 */
	void shuffleCards();
	/**
	 * A member method.
	 * Returns the number of cards left in the deck.
	 * @return	number of cards left in the deck
	 */
	int cardsLeft();
	/**
	 * A member method.
	 * Makes a copy of 1 card from the deck, removes the original card from the deck and then returns the copy.
	 * @return	last card from the cards_ vector.
	 */
	Card getCard();
	/**
	 * A member method.
	 * Returns size of the deck.
	 * @return	size of the deck
	 */
    int getSize(){return cards_.size(); }
private:
    /**
     * A private variable.
     * Holds the number of cards in the deck.
     */
	int cardsLeft_;
	/**
	 * A private variable.
	 * Holds the cards.
	 */
	std::vector<Card> cards_;
};


#endif /* CARD_DECK_HPP_ */
