#ifndef ERROR_HPP_
#define ERROR_HPP_
#include <exception>

/**
 * Class representing game runtime exception.
 * Object of this class is thrown when there is any runtime error in the game.
 */
class GameError:public std::exception{
public:
	/**
	 * Class constructor.
	 * Takes two parameters - exception message and id of the player who's move caused error
	 * @param	m	message of the exception
	 * @param	playerId	ID of the player who's action caused an error
	 */
	GameError(const std::string m, const int playerId): msg_(m),playerId_(playerId){}
	/**
	 * Class constructor.
	 * Takes one parameter - exception message.
	 * @param	m	message of the exception
	 */
	GameError(const std::string m):msg_(m),playerId_(0){}
	/**
	 * Copy constructor.
	 * Takes one parameter
	 * @param	e	Object of this classed to be copied
	 * @return	copied object
	 */
	GameError(const GameError& e) throw(): msg_(e.msg_), playerId_(e.playerId_){}
	/**
	 * Virtual destructor
	 */
	virtual ~GameError() throw() {};
	/**
	 * Member function.
	 * Return char pointer to the error message
	 * @return 	char pointer to the message
	 */
	const char* what(){return msg_.c_str();}
	/**
	 * Member function.
	 * Returns error message as string
	 * @return	error message
	 */
	std::string getMessage() const{return msg_;}
	/**
	 * Member function.
	 * Returns id of the player who caused an error
	 * @return	error message
	 */
	int getPlayerId() const {return playerId_;}
private:
	/**
	 * Private variable.
	 * Contains an error message
	 */
	std::string msg_;
	/**
	 * Private variable.
	 * Contains ID of the player who caused an error.
	 */
	int playerId_;
};


#endif
