#include "db_utils.hpp"
using namespace soci;
const size_t DBUtils::defPoolSize_ = 10;

DBUtils& DBUtils::getInstance() {
	static DBUtils instance;
	return instance;
}

DBUtils::DBUtils():pool_(DBUtils::defPoolSize_){}

void DBUtils::openConnections(std::string dbPath, int poolSize){
	using namespace soci;
	for (size_t i = 0; i != poolSize; ++i)
	{
		session & sql = pool_.at(i);
		sql.open(sqlite3,dbPath);
	}
}
