#include "table.hpp"

const std::string Table::tableStates[]  = {"waiting", "game", "finished"};
typedef boost::shared_ptr<Player> PlayerPtr;
typedef std::deque<PlayerPtr>::iterator PlayerIterator;



Table::Table(int tableId, int minPlayersNum,int maxPlayersNum):maxPlayersNum_(maxPlayersNum),
															    minPlayersNum_(minPlayersNum),
															    gameStateId_(0),
															    minBet_(2),
															    handNumber_(0),
															    theButton_(-1),
															    pot_(0),
															    handWinner_(-1),
															    tableId_(tableId),
															    tableState_(0),
															    roundAmount_(0)
																{}

void Table::newHand(){
	deck_.shuffleCards();
	cards_.clear();
	tableState_ = 1;
	roundAmount_=0;
	pot_=0;
	/* Save current players' accounts info to the database */
	saveStateToDatabase();
	for (PlayerIterator playIt = players_.begin(); playIt != players_.end(); ++playIt){
		(*playIt)->reset();
		/* Players who cannot afford to bet are set to be inactive */
		if((*playIt)->getMoney() <= minBet_){
			(*playIt)->playing(false);
		}
		/* We are handing 2 cards to each player */
		(*playIt)->addPlayerCard(deck_.getCard());
		(*playIt)->addPlayerCard(deck_.getCard());
	}
	int playersNum = players_.size();
	theButton_ = getNextPlayerPos(theButton_);

	PlayerPtr player = getNextPlayer(theButton_);
	smallBlind_.value_ = minBet_;
	smallBlind_.playerPosition_ = player->getTablePosition();
	addToPot(player,smallBlind_.value_);

	player = getNextPlayer(smallBlind_.playerPosition_);
	bigBlind_.value_ = minBet_*2;
	bigBlind_.playerPosition_ = player->getTablePosition();
	addToPot(player,bigBlind_.value_);

	/* Setting new current player */
	player = getNextPlayer(bigBlind_.playerPosition_);
	currentPlayerId_ = player->getId();
	currentPlayerPosition_ = player->getTablePosition();
	roundAmount_ = bigBlind_.value_;
	++handNumber_;
	roundNumber_=0;
	markModified();
}

/*
 * Helper function. Changes int to std::string.
 * @param	x	variable to be change
 * @return	string representing input variable
 */
std::string int2str(int x) {
    std::stringstream ss;
    ss << x;
    return ss.str();
}

std::string Table::getTableStatus(int playerId){
	using namespace boost;
	using namespace std;
	if (!isThere(playerId))
		throw GameError("No such player",playerId);
	boost::property_tree::ptree playersTree, playerCards, game, sBlind, bBlind, cardsOnTable, showdown;
	PlayerIterator it;
	/** Adding player */
	for (it = players_.begin(); it != players_.end(); ++it){
		boost::property_tree::ptree tmpPlayer;
		tmpPlayer.put<std::string>("nick", int2str((*it)->getId()) );
		tmpPlayer.put<int>("money", (*it)->getMoney());
		tmpPlayer.put<std::string>("state", (*it)->isPlaying() ? "playing" : "fold");
		playersTree.push_back(std::make_pair(int2str((*it)->getTablePosition()), tmpPlayer));
	}
	game.push_back(std::make_pair("players", playersTree));

	PlayerPtr playerP = getPlayer(playerId);

	std::vector<Card>::const_iterator cardIt;
	for(cardIt = playerP->getCards().begin(); cardIt !=playerP->getCards().end(); ++cardIt ){
		boost::property_tree::ptree tmpCard;
		tmpCard.put<std::string>("rank", cardIt->getRankStr() );
		tmpCard.put<std::string>("suit", cardIt->getSuitStr() );
		playerCards.push_back(std::make_pair("", tmpCard));
	}
	game.put<int>("tablePosition", playerP->getTablePosition());
	game.push_back(std::make_pair("myCards", playerCards));
	int minBet = roundAmount_ - (playerP->getBidPerRound());
	game.put<int>("minBid", minBet);
	game.put<int>("theButton", theButton_);
	sBlind.put<int>("amount",smallBlind_.value_);
	sBlind.put<int>("player",smallBlind_.playerPosition_);
	bBlind.put<int>("amount",bigBlind_.value_);
	bBlind.put<int>("player",bigBlind_.playerPosition_);
	game.push_back(std::make_pair("smallBlind", sBlind));
	game.push_back(std::make_pair("bigBlind", bBlind));

	game.put<int>("jackpot", pot_);
	game.put<int>("handNumber",handNumber_);
	game.put<int>("currentPlayer", currentPlayerPosition_);
	game.put<int>("round", roundNumber_);

	for(cardIt = cards_.begin(); cardIt !=cards_.end(); ++cardIt ){
		boost::property_tree::ptree tmpCard;
		tmpCard.put<std::string>("rank", cardIt->getRankStr() );
		tmpCard.put<std::string>("suit", cardIt->getSuitStr() );
		cardsOnTable.push_back(std::make_pair("", tmpCard));
	}
	game.push_back(std::make_pair("cardsOnTable", cardsOnTable));
	game.put<int>("winner",handWinner_);
	game.put<std::string>("tableState",Table::tableStates[tableState_]);

    std::stringstream ss;
    write_json(ss, game);
    std::string my_string = ss.str();
    return my_string;
}


bool Table::executePlayerAction(int playerId, int playerAction, unsigned long bid){
	uniqueLock lock(tableAccess_);
	PlayerPtr player;
	if(playerId != currentPlayerId_)
		throw GameError("Not your turn to move",playerId);
	if (!isThere(playerId)){
		/* Set another player as current player */
		player = getNextPlayer(currentPlayerPosition_);
		currentPlayerId_ = player->getId();
		throw GameError("Player is not assigned to the table",playerId);
	}
	player = getPlayer(playerId);
	if(playerAction == 0){		/* fold */
		player->makeMove(true);
		player->playing(false);
	}
	else if (playerAction == 1){
		if(roundAmount_ != (player->getBidPerRound()) )
			throw GameError("You cannot check",playerId);
		else
			player->makeMove(true);
	}else{
		if(bid < (roundAmount_ - (player->getBidPerRound())) ){
			throw GameError("Your bet is too small",playerId);
		}
		else{
			addToPot(player,bid);
			if(player->getBidPerRound() > roundAmount_)
				roundAmount_ = player->getBidPerRound();
			player->makeMove(true);
		}
	}

	if(betsFinished()){ /* The end of the round */
		++roundNumber_;
		roundAmount_=0;
		for (PlayerIterator playIt = players_.begin(); playIt != players_.end(); ++playIt){
			(*playIt)->resetRound();
		}
		if(roundNumber_>0){ //rundę zaczyna gracz na lewo od dealera
			PlayerPtr nextPlayer = getNextPlayer(theButton_);
			currentPlayerId_ = nextPlayer->getId();
			currentPlayerPosition_ = nextPlayer->getTablePosition();
		}
		/* Card on table */
		if (roundNumber_==1){ //3 cards on table
			for (int i=0; i!=3 ;++i){
				cards_.push_back(deck_.getCard());
			}
		}
		else if(roundNumber_==2 || roundNumber_==3){
			cards_.push_back(deck_.getCard());
		}
		else if(roundNumber_==4){ /* The end of current hand */
			/* Checking cards */
			std::cout << "koniec hand'a" << std::endl;
			handWinner_ = checkCards();
			PlayerPtr winnerPlayer = getNextPlayer(handWinner_);
			winnerPlayer->addMoney(pot_);
			tableState_=2;
			//tmp
			for (PlayerIterator it=players_.begin(); it!=players_.end(); ++it)
				std::cout << "player id "<<(*it)->getId() << " figure " <<(*it)->getHandFigure() <<std::endl;
			//tmp
			std::cout << "winner is player " << winnerPlayer->getId() <<  std::endl;
			newHand();
		}
	}
	else{
		PlayerPtr nextPlayer = getNextPlayer(player-> getTablePosition());
		currentPlayerId_ = nextPlayer->getId();
		currentPlayerPosition_ = nextPlayer->getTablePosition();
	}

	markModified();
}


bool Table::addPlayerToTable(PlayerPtr player) {
	uniqueLock lock(tableAccess_);
	int playerId = player->getId();
	if( (players_.size() + 1) > players_.max_size() )
		throw GameError("Too many players at the table",playerId);
	/* Checking if this player is already assigned to this table */
	if(isThere(playerId))
		throw GameError("Player is already at the table",playerId);
	/* Checking if there is a free place at this table */
	if (players_.size() >= maxPlayersNum_)
		throw GameError("Too many players at the table",playerId);
	/* Player is not active till the next hand */
	player->playing(false);
	/* Setting player's table id */
	player->setTableId(tableId_);
	/* Choosing free position at the table for the new player (random or first free)*/
	player->setTablePosition(randomFreeSpot());
	players_.push_back(player);
	/* Sorting players by their position at the table */
	sortByPosition();
	markModified();
	/* If there is enough players (minPlayersNum_), we can start playing */
	if(players_.size() == minPlayersNum_)
		newHand();
	return true;
}

bool Table::isThere(int playerId) {
	if(getPlayerIter(playerId)==players_.end()){
		return false;
	}
	return true;
}

bool Table::removePlayerFromTable(int playerId) {
	uniqueLock lock(tableAccess_);
	if(!isThere(playerId))
		throw GameError("Player is not assigned to the table",playerId);
	getPlayer(playerId)->leaveTable();
	players_.erase(std::remove_if(players_.begin(), players_.end(),
			boost::bind(std::equal_to<int>(), boost::bind(&Player::getId,_1), playerId) ), players_.end());
	markModified();
	if (players_.size() < minPlayersNum_)
		tableState_ = 0;
	return true;
}

int Table::countPlayers() {
	sharedLock lock(tableAccess_);
	return players_.size();
}

inline void Table::markModified() {
	gameStateId_++;
}

Table::gameStateId_t Table::getLastModified() {
	sharedLock lock(tableAccess_);
	return gameStateId_;
}

void Table::removeAllIdleFor(int seconds) {
	uniqueLock lock(tableAccess_);
	typedef boost::posix_time::time_duration p_time;
	int playerId;
	p_time duration = boost::posix_time::seconds(seconds);
	for (PlayerIterator it = players_.begin(); it != players_.end(); ++it)
		if (it->get()->isIdleFor(duration)) {
			playerId = it->get()->getId();
			it->get()->leaveTable();
			players_.erase(
					std::remove_if(players_.begin(), players_.end(),
							boost::bind(std::equal_to<int>(), boost::bind(&Player::getId, _1),
									playerId)), players_.end());
			markModified();
		}
	if (players_.size() < minPlayersNum_)
		tableState_ = 0;
}

/**
 * Helper function for comparing two playing cards.
 */
bool compareCards (Card const &card1,Card const &card2) {
    return (card1.getRank() > card2.getRank() );
}

void Table::fillHand(std::vector<Card> &allCards, std::vector<Card> &bestHand, int cardsNum){
    bool found = false;
    int added = 0;
    for (int i = 0; i != 7; i++){
        found = false;
        for (int j=0; j!=bestHand.size() ; j++){
            if(bestHand[j] == allCards[i]){
                found = true;
                break;
            }
        }
        if (!found){
            bestHand.push_back(allCards[i]);
            added++;
        }
        if (added == cardsNum)
            break;
    }
}

int Table::evaluateHand(std::vector<Card> allCards, std::vector<Card> &bestHand){
	std::sort(allCards.begin(), allCards.end(), compareCards);
	int straight,flush,three,four,full,pairs;
	int k;
	int cardsNum=7;
	straight = flush = three = four = full = pairs = 0;
	k = 0;

	for (int i=0;i<3;i++){
		k=i;
		while (k<4&&allCards[k].getRank()==allCards[k+1].getRank()-1)
			k++;
		if (k==4){
			straight = 1;
			for (int p=i; p != (i+5) ; ++p)
				bestHand.push_back(allCards[p]);
			int q=i;
			while (q<4&&allCards[q].getSuit()==allCards[q+1].getSuit())
				q++;
			if (q==4){
				return 9;
			}
			break;
		}
	}
	/* Checks for fours */
	for (int i=0;i<4;i++){
		k = i;
		while (k<i+3&&allCards[k].getRank()==allCards[k+1].getRank())
			k++;
		if (k==i+3){
			four = 1;
			if (!bestHand.empty())
				bestHand.clear();
			for (int p=i; p != (i+4) ; ++p)
				bestHand.push_back(allCards[p]);
			fillHand(allCards,bestHand,1);
			return 8;
		}
	}
	/* Check for flush */
	int cardsSuits[] = {0,0,0,0};
	for (int i=0;i<cardsNum;i++){
		cardsSuits[allCards[i].getSuit()]++;
	}
	for (int i=0;i<4;i++){
		if(cardsSuits[i] >= 5){
			flush=1;
			if (!bestHand.empty())
				bestHand.clear();
			for (int p=0; p != (i+7) ; ++p){
				if(allCards[p].getSuit() == i)
					bestHand.push_back(allCards[p]);
			}
		}
	}
	/* Checks for threes and full house */
	if (!four){
		for (int i=0;i<5;i++){
			k = i;
			while (k<i+2&&allCards[k].getRank()==allCards[k+1].getRank())
				k++;
			if (k==i+2){    //jest trójka
				three = 1;
				for (int j=(k+1); j<(k+4) ;j++){
					int l=j%(7);
					if (allCards[l].getRank() == allCards[l+1].getRank()){
						if (!bestHand.empty())
							bestHand.clear();
						for (int p=i; p != (i+3) ; ++p)
							bestHand.push_back(allCards[p]);
						full=1;
						bestHand.push_back(allCards[l]);
						bestHand.push_back(allCards[l+1]);
						return 7;
						//break;
					}
				}
				if(flush)
					return 6;
				if(straight)
					return 5;
				if (!bestHand.empty())
					bestHand.clear();
				for (int p=i; p != (i+3) ; ++p)
					bestHand.push_back(allCards[p]);
			 		/* Adding 3 best cards from the cards that are left */
					fillHand(allCards,bestHand,2);
				return 4;
			}

		}
	}
	/* Checks for pairs */
	if (!bestHand.empty())
		bestHand.clear();
	for (k=0;k<6;k++){
		if (allCards[k].getRank()==allCards[k+1].getRank()){
			pairs++;
			bestHand.push_back(allCards[k]);
			bestHand.push_back(allCards[k+1]);
		}
		if (pairs == 2)
			break;
	}
	if (pairs==2){
		std::sort(bestHand.begin(), bestHand.end(), compareCards);  // TODO: użyć boost::bind!!
		fillHand(allCards,bestHand,1);
		return 3;
	}
	else if(pairs==1){
		fillHand(allCards,bestHand,3);
		return 2;
	}
	else{
		fillHand(allCards,bestHand,5);
		return 1;
	}
}

int Table::compareTwoHands(PlayerPtr p1, PlayerPtr p2){
	if(p1->getBestHand().size() !=5 && p2->getBestHand().size() != 5){} 			// RZUCAĆ WYJĄTKIEM!!!!!!!!
	if(p1->getHandFigure() > p2->getHandFigure())
		return -1;
	else if (p1->getHandFigure() > p2->getHandFigure())
		return 1;
	else{
		for (int i=0; i!=5; ++i){
			if(p1->getBestHand()[i]<p2->getBestHand()[i])
				return 1;
			if(p1->getBestHand()[i]>p2->getBestHand()[i])
				return -1;
		}
	}
	return 0;
}

int Table::checkCards(){
	PlayerIterator currentWinner ;
	std::vector<Card> allCards(cards_);
	PlayerIterator playerIt;
	for (playerIt = players_.begin(); playerIt != players_.end(); ++playerIt){
		if((*playerIt)->isPlaying()){
			while(allCards.size() != 5)
				allCards.pop_back();
			allCards.insert(allCards.end(),(*playerIt)->getCards().begin(),(*playerIt)->getCards().end() );
			(*playerIt)->setHandFigure(evaluateHand(allCards, (*playerIt)->getBestHand()));
			if(playerIt == players_.begin() || compareTwoHands(*playerIt,*currentWinner)==-1){
				currentWinner = playerIt;
			}
		}
	}
	return (*currentWinner)->getId();
}

void Table::sortByPosition(){
	std::sort(players_.begin(), players_.end(), boost::bind(std::less<int>(), boost::bind(&Player::getTablePosition,_1), bind(&Player::getTablePosition,_2)) );
}

PlayerPtr Table::getPlayer(int playerId) {
	return *(std::find_if(players_.begin(), players_.end(),boost::bind(std::equal_to<int>(), boost::bind(&Player::getId,_1), playerId)));
}

PlayerPtr Table::getPlayerByPos(int playerPosition) {
	return *(std::find_if(players_.begin(), players_.end(),boost::bind(std::equal_to<int>(), boost::bind(&Player::getTablePosition,_1), playerPosition)));
}

PlayerIterator Table::getPlayerIter(int playerId) {
	return (std::find_if(players_.begin(), players_.end(),boost::bind(std::equal_to<int>(), boost::bind(&Player::getId,_1), playerId)));
}


bool compOnlyActive(int val, const PlayerPtr &playPtr){
    if(!playPtr->isPlaying())
        return false;
    return val < playPtr->getTablePosition();
}

PlayerPtr Table::getNextPlayer(int prevPlayerPos) {
	PlayerIterator it  = upper_bound (players_.begin(), players_.end(), prevPlayerPos, boost::bind(compOnlyActive,_1,_2));
	if(it==players_.end())
		it = upper_bound (players_.begin(), players_.end(), -1, boost::bind(compOnlyActive,_1,_2));
	return *it;
}

int Table::getNextPlayerPos(int prevPlayerPos){
	PlayerIterator it  = upper_bound (players_.begin(), players_.end(), prevPlayerPos, boost::bind(compOnlyActive,_1,_2));
	if(it==players_.end())
		it = upper_bound (players_.begin(), players_.end(), -1, boost::bind(compOnlyActive,_1,_2));
	return (*it)->getTablePosition();
}

void Table::addToPot(PlayerPtr player, int money){
	player->addMoneyToPot(money);
	pot_+=money;
}

bool Table::betsFinished(){
	PlayerIterator it;
	PlayerIterator it2;
	it = players_.begin();
	while(it != players_.end()){
		if ((*it)->isPlaying())
		{
			if (!(*it)->hasMoved())
				return false;
			it2 = it+1;
			if(it2 != players_.end() && (*it)->getBidPerRound() != (*it2)->getBidPerRound())
				return false;
		}
		++it;
	}
	return true;
}

typedef boost::mt19937 RNGType;
RNGType rng (time(0) );

int Table::randomFreeSpot(){
	PlayerIterator it;
	std::vector<int> emptySpots(maxPlayersNum_);
	for (int i=0; i!= maxPlayersNum_; ++i)
		emptySpots[i] = i;
	for (it = players_.begin(); it != players_.end(); ++it){
		emptySpots[(*it)->getTablePosition()] = -1;
	}
	emptySpots.erase(std::remove(emptySpots.begin(), emptySpots.end(), -1), emptySpots.end());
	return emptySpots[ rng() % emptySpots.size()];
}

void Table::saveStateToDatabase(){
	using namespace soci;
	try{
		/* get one connection from connections pool */
		session sql(DBUtils::getInstance().getConnPool());
		PlayerIterator it;
		/* save players' account information to the database */
		for ( it=players_.begin(); it!=players_.end(); ++it){
		   sql << "update auth_user"
			  " set money = :money"
			  " where id = :id",
			  use((*it)->getMoney()),use((*it)->getId());
		}
	}
	catch (std::exception const &e)
	{
		std::cerr << "Database may be locked (ohh SQLite..): " << e.what() << '\n';
	}
}
