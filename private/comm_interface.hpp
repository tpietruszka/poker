#ifndef COMM_INTERFACE_HPP_
#define COMM_INTERFACE_HPP_

#include <string>
/**
 * Abstract class.
 * This is an abstract class which defines functions needed to communicate with web2py.
 * All classes that want to communicate with web2py (i.e. other games) should implement this interface.
 */
class ICommInterface{
public:
	typedef unsigned long long gameStateId_t;
	/**
	 * A pure virtual member function.
	 * Creates JSON string representing the state of player's table.
	 * @param	playerId	ID of the player that sends requested GameStatus.
	 * @return	JSON string representing game status
	 */
	virtual std::string getGameStatus(int playerId) = 0;
	/**
	 * A pure virtual member function.
	 * Executes player's action. It is called when player made a move in the game.
	 * @param	playerId		ID of the player that made a move
	 * @param	playerAction	number representng player's action
	 * @param	bid				amount of money a plyer would like to bid
	 * @return	true is player succeed in finishing his move
	 */
	virtual bool executePlayerAction(int playerId, int playerAction, unsigned long bid) = 0;
	/**
	 *  A pure virtual member function.
	 * Returns value of the last modification of table status. Value representing last modification
	 * is incremented every time something on the table changes.
	 * @param	playerId	ID of the player that requested the value representing last modification
	 * @return	value representing the last modification of the table state.
	 */
	virtual gameStateId_t getLastModified(int playerId) = 0;
	/**
	 * A pure virtual member function.
	 * Saves information about the fact that player made a request to the server.
	 * @param	playerId	ID of the player that made a request
	 */
	virtual void markPlayerRequest(int playerId) = 0;
	/**
	 * A pure virtual member function.
	 * Removes all idle players from the table.
	 * @param	seconds		after this time all inactive players will be removed
	 */
	virtual void removeAllIdleFor(int seconds) = 0;
};


#endif /* COMM_INTERFACE_HPP_ */
