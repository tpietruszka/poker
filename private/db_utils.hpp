#ifndef DBUTILS_HPP_
#define DBUTILS_HPP_
#include <soci.h>
#include <soci-backend.h>
#include <boost-optional.h>
#include <sqlite3/soci-sqlite3.h>

/**
 * Helper class used for connecting to the sqlite database. It implements Singleton pattern.
 * It uses SOCI library to create pool of database connection that can be used by different threads.
 */
class DBUtils {
public:
	/**
	 * Public member function.
	 * Return reference to the create instance of the object.
	 * @return	reference to the instance of the DBUtils object
	 */
	static DBUtils& getInstance();
	/**
	 * Public member function.
	 * Take two arguments.
	 * Creates pool of connections to the database. Number if connections is set by input argument.
	 * @param	dbPath	string representing path to the database file
	 * @param	poolSize	number of connections in the pool
	 */
	void openConnections(std::string dbPath, int poolSize);
	/**
	 * Public member function.
	 * Returns reference to the pool object containing connections.
	 * @return	SOCI pool object with connections
	 */
	soci::connection_pool &getConnPool(){return pool_;}
private:
	/**
	 * Private default constructor.
	 */
	DBUtils();
	/**
	 * Private copy constructor.
	 */
	DBUtils(const DBUtils&);
	/**
	 * Private static member variable.
	 * Represent default number of connections in the pool. Used by default constructor.
	 */
	static const size_t defPoolSize_;
	/**
	 * Private member variable.
	 * Represents pool of connections to the database.
	 */
	soci::connection_pool pool_;

};

#endif

