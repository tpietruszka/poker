/**
 *  Defines functions that are exposed to the Python interpreter by Boost::Python library
 */
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/exception_translator.hpp>
#include <exception>
#include "texas_holdem.hpp"
#include "error.hpp"

/**
 * Static function.
 * Translates C++ exception type to Python Exception.
 * @param	err		Exception object thrown by the C++
 */
static void MyExceptionTranslator(const GameError &err) {
	PyErr_SetString(PyExc_UserWarning, err.getMessage().c_str());
}
/**
 * Boost python module exposing C++ classes to Python using Boost Ptyhon library.
 */
BOOST_PYTHON_MODULE(pokergame)
{
	typedef std::vector<int> MyList;
    using namespace boost::python;
    register_exception_translator<GameError>(&MyExceptionTranslator);

    class_<MyList>("MyList")
            .def(vector_indexing_suite<MyList>() );

    class_<TexasHoldem, boost::noncopyable>("TexasHoldem")
				.def(init<std::string>())
				.def("get_table_ids", &TexasHoldem::getTableIds)
				.def("create_table", &TexasHoldem::createTable)
				.def("delete_table", &TexasHoldem::deleteTable)
				.def("get_game_status", &TexasHoldem::getGameStatus)
		        .def("execute_player_action", &TexasHoldem::executePlayerAction)
		        .def("get_last_modified", &TexasHoldem::getLastModified)
		        .def("mark_player_request", &TexasHoldem::markPlayerRequest)
		        .def("remove_all_idle_for", &TexasHoldem::removeAllIdleFor)
		        .def("add_player", &TexasHoldem::addPlayer)
				.def("join_table", &TexasHoldem::joinTable)
				.def("remove_player", &TexasHoldem::removePlayer)
				.def("leave_table", &TexasHoldem::leaveTable)
				.def("count_all_players", &TexasHoldem::countAllPlayers)
				.def("count_table_players", &TexasHoldem::countTablePlayers)
				.def("get_player_position", &TexasHoldem::getPlayerPosition)
        ;
}

