#include "card.hpp"

const std::string Card::suits[]  = {"hearts", "spades", "diamonds", "clubs"};
const std::string Card::ranks[] = {"2","3","4","5","6","7","8","9","10","jack","queen","king", "ace"};

Card::Card(int suit, int rank):suit_(suit),rank_(rank)
{}

bool operator== (Card &c1, Card &c2)
{
    return (c1.rank_ == c2.rank_ && c1.suit_ == c2.suit_ );
}

bool operator< (Card &c1, Card &c2)
{
    return (c1.rank_ < c2.rank_ );
}

bool operator> (Card &c1, Card &c2)
{
    return (c1.rank_ > c2.rank_ );
}
