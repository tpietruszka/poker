#ifndef PLAYER_HPP_
#define PLAYER_HPP_
#include <boost/date_time/posix_time/posix_time.hpp>
#include "card.hpp"
#include "error.hpp"
/**
 * Class representing player in the game. Player object is created once, when he joins the game.
 * When player joins a table, new shared_ptr jest created pointing to player's objects and is stored in the
 * table object.
 */
class Player {

public:
	/**
	 * Constructor with 2 arguments.
	 * @param	player_id	Id of the player from the database table auth_user
	 * @param	money	user's money from the database table auth_user
	 */
	Player(int player_id, double money);
	/**
	 * Public member function.
	 * Accessor used to return id of the player
	 * @return	player's id
	 */
	int getId(){return id_;}
	/**
	 * Public member function.
	 * Marks user's request by changing the time of the last request.
	 */
	void markRequest();
	/**
	 * Public member function
	 * Checks for how long the player has been idle.Check if the time that has passed from last request is
	 * greater or equal 'duration'.
	 * @param	duration	time to be compared with the time that passed from the last request
	 * @return	true - if player has been idle for at least 'duration' time, false - otwerwise
	 */
	bool isIdleFor(boost::posix_time::time_duration &duration);
	/**
	 * Overridden comparison operator.
	 * Operator "equal to" checks if objects are equal. Objects are equal when their id_ variable are equal.
	 * @return	true - if object are equal.
	 */
	bool operator==(int player_id);
	/**
	 * Public member function.
	 * Accessor returning ID of the table a player is assigned to.
	 * @return	id of the table.
	 */
	int getTableId(){return tableId_;}
	/**
	 * Public member function.
	 * Mutator setting player's table id. It is called when player joins a table.
	 * @param	tableId		Id of the table player wants to join.
	 */
	void setTableId(int tableId){tableId_ = tableId;}
	/**
	 * Public member function.
	 * Accessor returning variable isPlaying_.
	 * @return	true if player is still playering, false if player folded.
	 */
	bool isPlaying(){return isPlaying_;}
	/**
	 * Public member function.
	 * Accessor returning reference to variable playerCards_.
	 * @return	playerCards_ variable.
	 */
	const std::vector<Card>& getCards(){return playerCards_;}
	/**
	 * Public member function.
	 * Accessor returning reference to variable bestHand_.
	 * @return	bestHand_ variable.
	 */
	std::vector<Card>& getBestHand() {return bestHand_;}
	/**
	 * Public member function.
	 * Mutator setting variable 'handFigure_'.
	 * @param	value to set to handFigure_.
	 */
	void setHandFigure(int handFigure){handFigure_ = handFigure;}
	/**
	 * Public member function.
	 * Accessor returning variable handFigure_.
	 * @return	handFigure_ variable.
	 */
	int getHandFigure(){return handFigure_;}
	/**
	 * Public member function.
	 * Mutator setting variable 'tablePosition_'
	 * @param	value to set to tablePosition_.
	 */
	void setTablePosition(int tablePosition){tablePosition_ = tablePosition;}
	/**
	 * Public member function.
	 * Accessor returning variable tablePosition_.
	 * @return	tablePosition_ variable.
	 */
	int getTablePosition() {return tablePosition_;}
	/**
	 * Public member function.
	 * Subtracts from player's money amount of money that player put in the pot.
	 * Adds  amount of money that player put in the pot to the bidPerRound_ variable.
	 * @param	value	amount of money player put in the pot
	 * @return	true if operations succeeded, false if player didn't have enough money to subtract from.
	 */
	bool addMoneyToPot(int value);
	/**
	 * Public member function.
	 * Accessor returning variable money_.
	 * @return	money_ variable.
	 */
	int getMoney(){return money_;}
	/**
	 * Public member function.
	 * Adds money that player won to his money_ variable.
	 * @param	m	amount of money that player won.
	 */
	void addMoney(int m){money_+=m;}
	/**
	 * Public member function.
	 * Adds new card from deck to player's playerCards_ variable during flop.
	 * @param	card	Card that player was given and is added to player's card.
	 */
	void addPlayerCard(Card card){playerCards_.push_back(card);}
	/**
	 * Public member function.
	 * Mutator setting variable 'isPlaying_'.
	 * @param	value to set to isPlaying_.
	 */
	void playing(bool isPlaying){isPlaying_ = isPlaying;}
	/**
	 * Public member function.
	 * Accessor returning variable hasMoved_.
	 * @return	hasMoved_ variable.
	 */
	bool hasMoved(){return hasMoved_;}
	/**
	 * Public member function.
	 * Mutator setting variable 'hasMoved_'.
	 * @param	value to set to hasMoved_.
	 */
	void makeMove(bool move){hasMoved_ = move;}
	/**
	 * Public member function.
	 * Function resets player's variables to default values when he leaves the table.
	 */
	void leaveTable();
	/**
	 * Public member function.
	 * Accessor returning variable bidPerRound_.
	 * @return	bidPerRound_ variable.
	 */
	int getBidPerRound(){return bidPerRound_;}
	/**
	 * Public member function.
	 * Function resets some of the player's variables to default values when the new hand starts.
	 */
	void reset();
	/**
	 * Public member function.
	 * Function resets some of the player's variables to default values when the round starts.
	 */
	void resetRound(){hasMoved_=false; bidPerRound_=0;}
private:
	/**
	 * Private member variable.
	 * It is set to 'True' if player has already made a move in this round of hand.
	 * It is set to 'False' at the beginning of each round.
	 */
	bool hasMoved_;
	/**
	 * Private member variable.
	 * It is set to 'False' if player had folded. If the player is still in the game,
	 * it is set to 'True'. It is set to 'True' at the beginning of each hand. When player joins
	 * the table it is set to 'False' as player has to wait for the new hand.
	 */
	bool isPlaying_;
	/**
	 * Private member variable.
	 * Vector containing two cards that player receives during flop.
	 */
	std::vector<Card> playerCards_;
	/**
	 * Private member variable.
	 * When cards are checked at the end of the game this vector contains 5 cards (out of 7)
	 * which represent player's best hand.
	 */
	std::vector<Card> bestHand_;
	/**
	 * Private member variable.
	 * Id of the table that player is assigned to.
	 */
	int tableId_;
	/**
	 * Private member variable.
	 * Player's ID. It is equal to the id column i auth_user table in users' database.
	 */
	int id_;
	/**
	 * Private member variable.
	 * Time of the user's last request. It is used to check if player is not offline.
	 */
	boost::posix_time::ptime lastRequest_;
	/**
	 * Private member variable.
	 * Value representing best figure that player's bestHand_ vector represents.
	 * Values correspond to the elements of the figuresNames_ array
	 */
	int handFigure_;
	/**
	 * Private member variable.
	 * Player's money. When player joins a game, this value is withdrawn from the database.
	 * Player's current amount of money is saved to the database at the beginning of each hand.
	 */
	double money_;
	/**
	 * Private member variable.
	 * Player's position at the table. Player is assigned a random position at the table
	 * when he joins a table.
	 */
	int tablePosition_;
	/**
	 * Private member variable.
	 * Amount of money that a player has put in the pot in this round.
	 * It is used to check if everyone put the same amount of money in the pot and
	 * we can move to the next round. It is set to 0 at the beginning of new round.
	 */
	int bidPerRound_;
	/**
	 * Private static array.
	 * Stores names of all the possible poker figures.
	 */
	static const std::string figuresNames_[];
};

#endif /* PLAYER_HPP_ */
