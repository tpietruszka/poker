#include "card_deck.hpp"
#include <iostream>

CardDeck::CardDeck():cardsLeft_(52){
	shuffleCards();
}

Card CardDeck::getCard(){
    Card tmp(cards_.back());
    cards_.pop_back();
    --cardsLeft_;
    return tmp;
}

void CardDeck::shuffleCards(){
	using namespace std;
	cardsLeft_=52;
	cards_.clear();
	for (int a=0; a<4; a++)
	{
		for (int b=0; b<13; b++)
		{
			cards_.push_back(Card(a,b));
		}
	}
    std::random_shuffle(cards_.begin(),cards_.end());
}
