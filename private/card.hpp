#ifndef CARD_HPP_
#define CARD_HPP_
#include <string>

/**
 * A card class.
 * Represents a card in poker game.
 */
class Card{
public:
	/**
	 * A constructor.
	 * Constructor with 2 parameters.
	 * @param	suit	suit of a card represented as an index of suit array
	 * @param	rank	rank of a card represented as an index of ranks array
	 */
	Card(int suit, int rank);
	/**
	 * A public member method.
	 * Returns a number representing a rank of the card.
	 * @return	number representing a rank of the card
	 */
	int getRank() const {return rank_;}
	/**
	 * A public member method.
	 * Returns a number representing a suit of the card.
	 * @return	number representing a suit of the card
	 */
	int getSuit() const {return suit_;}
	/**
	 * A public member method.
	 * Returns a string representing a rank of the card.
	 * @return	string representing a rank of the card
	 */
	std::string getRankStr() const {return ranks[rank_];}
	/**
	 * A public member method.
	 * Returns a string representing a suit of the card.
	 * @return	string representing a suit of the card
	 */
	std::string getSuitStr() const {return suits[suit_];}
	/**
	 * A public member method.
	 * Returns a string representing a suit and a rank of the card.
	 * @return	string representing a suit and a rank of the card.
	 */
    std::string toString() {return suits[suit_] + std::string("  ") + ranks[rank_];}
    /**
	 * Overloaded "equal to" operator.
	 * Compares ranks and suits of two cards and return true value if they are equal.
	 * @param	c1	reference to the first card object to be compared
	 * @param	c2	reference to the second card object to be compared
	 * @return	true - if ranks and suits of two cards are equal, false - otherwise
	 */
    friend bool operator== (Card &c1, Card &c2);
    /**
   	 * Overloaded "less than" operator.
   	 * Compares only ranks of two cards.
   	 * @param	c1	reference to the first card object to be compared
	 * @param	c2	reference to the second card object to be compared
   	 * @return	true - if rank of the first card is smaller than the rank of the second card,false - otherwise
   	 */
    friend bool operator< (Card &c1, Card &c2);
    /**
	 * Overloaded "greater than" operator.
	 * Compares only ranks of two cards.
	 * @param	c1	reference to the first card object to be compared
	 * @param	c2	reference to the second card object to be compared
	 * @return	true - if rank of the first card is bigger than the rank of the second card,false - otherwise
	 */
    friend bool operator> (Card &c1, Card &c2);
private:
	/**
	 * A private static array.
	 * Contains all possible suit names of the cards.
	 */
    static const std::string suits[];
    /**
	 * A private static array.
	 * Contains all possible rank names of the cards.
	 */
	static const std::string ranks[];
	/**
	 * A private variable.
	 * Represents card's rank as an index of ranks array.
	 */
	int rank_;
	/**
	 * A private variable.
	 * Represents card's suit as an index of suits array.
	 */
	int suit_;
};


#endif /* CARD_HPP_ */
