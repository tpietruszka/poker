#ifndef TEXAS_HOLDEM_HPP_
#define TEXAS_HOLDEM_HPP_
#include <string>
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>
#include <boost/thread.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/assign.hpp>
#include "db_utils.hpp"
#include "comm_interface.hpp"
#include "table.hpp"
/**
 * Class representing main game object. There is only one instance of TexasHoldem.
 * Object of this class stores pointer to all the created tables and player who are logged in to the game.
 * It implements ICommInterface communication interface. All the requests from the players are forwarded
 * to the tables using this class.
 *
 */
class TexasHoldem : public ICommInterface{
public:
	typedef boost::unique_lock<boost::shared_mutex> uniLock;
	typedef unsigned long long gameStateId_t;
	typedef boost::shared_ptr<Player> PlayerPtr;
	typedef boost::shared_ptr<Table> TablePtr;
	/**
	 * Class constructor.
	 */
	TexasHoldem();
	/**
	 * Class constructor with 1 argument
	 * @param	dbPath	path to the database with players' information
	 */
	TexasHoldem(std::string dbPath);
	/**
	 * Public member function
	 * Return all the table IDs from the tables_ map.
	 * @return	vector with IDs of the tables created in this instance of game
	 */
	std::vector<int> getTableIds();
	/**
	 * Public member function
	 * Creates new table object and puts shared pointer to this object to tables_ vector
	 * @param	minPlayersNum	minimum number of player that have to join table for the game to start
	 * @param	maxPlayersNum	maximum number of players that can join the table
	 * @return	true if table was created, otherwise false
	 */
	bool createTable(int minPlayersNum,int maxPlayersNum);
	/**
	 * Public member function
	 * Deletes table from the game.
	 * @param	tableId		Id of the table that will be deleted
	 * @return	true if table was successfully deleted, otherwise false
	 */
	bool deleteTable(int tableId);
	/**
	 * Public member function
	 * Return JSON string with the game status for player's table.
	 * @param	playerId	ID of the payer who requested his table status
	 * @return	JSON String representing table status
	 */
	std::string getGameStatus(int playerId);
	/**
	 * Public member function.
	 * Executes player's action. When user send request to the server this function checks if it's his turn
	 * to make o move. If it's his turn function makes changes to the table according to the provided
	 * arguments.
	 * @param	playerId	ID of the player who made a move
	 * @param	playerAction	number representing action made by player
	 * @param	bid			money that player put to the pot.
	 */
	bool executePlayerAction(int playerId, int playerAction, unsigned long bid);
	/**
	 * Returns current game state id - the id created during last game modification.
	 * @return		current game state id.
	 */
	gameStateId_t getLastModified(int playerId);
	/**
	 * Public member function
	 * Marks user's request by changing the time of the last request.
	 * @param	playerId	ID of the player whose request time we are chaging
	 */
	void markPlayerRequest(int playerId);
	/**
	 * Public member function
	 * Revoves all inactive players from the game.
	 * @param	seconds		afters this time inactive players will be removed
	 */
	void removeAllIdleFor(int seconds);
	/**
	 * Public member function
	 * Adds new player to the game. It gets player's info from the database, based on player's ID
	 * @param	playerId	ID of the player we want to add to the game
	 * @return	true if player was added, false otherwise
	 */
	bool addPlayer(int playerId);
	/**
	 * Public member function
	 * Adds player to the table.
	 * @param	playerId	ID of the player we want to add to the table
	 * @param	tableId		ID of the table we want to add player to
	 * @return	true if player was added, false otherwise
	 */
	bool joinTable(int tableId, int playerId);
	/**
	 * Public member function
	 * Removes player from the game
	 * @param	playerId	ID of the player we want to remove from the game
	 * @return	true if player was deleted, false otherwise
	 */
	bool removePlayer(int playerId);
	/**
	 * Public member function
	 * Removes player from the table
	 * @param	playerId	ID of the player we want to remove from the table
	 * @return	true if player was deleted, false otherwise
	 */
	bool leaveTable(int playerId);
	/**
	 * Public member function
	 * Count all players in the game.
	 * @return	number of players in the game
	 */
	int countAllPlayers();
	/**
	 * Public member function
	 * Count players assigned to the specific table.
	 * @param	tableId		ID of the table where we want to count players
	 * @return	number of players in the table
	 */
	int countTablePlayers(int tableId);
	/**
	 * Public member function
	 * Accessor returning position of the player
	 * @param	playerId	ID of the player we want to retrieve positon
	 * @return 	player's position
	 */
	int getPlayerPosition(int playerId);

private:
	/**
	 * Private member function
	 * Gets player's account from database.
	 * @param	playerId	ID of the player
	 * @return	player's money from database
	 */
	double getMoneyFromDB(int playerId);
	/**
	 * Private member function.
	 * Checks if player is logged into the game.
	 * @param	playerId 	ID of the player we are looking for
	 * @return	true if player is logged in, false if he is not
	 */
	bool isLogged(int playerId);
	/**
	 * Private variable
	 * Map containing all players.
	 */
	std::map<int,PlayerPtr> players_;
	/**
	 * Private variable
	 * Map containing all tables.
	 */
	std::map<int,TablePtr> tables_;
	/**
	 * Private variable
	 * Mutex used to block all game.
	 */
	boost::shared_mutex mutex_;
	/**
	 * Private variable
	 * First available id for the new table.
	 */
	int nextTableId_;
	/**
	 * Private variable
	 * Path to the database file.
	 */
	std::string dbPath_;
};


#endif /* TEXAS_HOLDEM_HPP_ */
