#ifndef TABLE_HPP_
#define TABLE_HPP_
#include <string>
#include <iterator>
#include <list>
#include <algorithm>
#include <boost/random.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <exception>
#include "player.hpp"
#include "error.hpp"
#include "card_deck.hpp"
#include "card.hpp"
#include "db_utils.hpp"

/**
 * Helper class representing blinds in the game.
 */
struct Blind{
	/**
	 * Value of the blind (amount of money that player who has the blind has to put in the pot).
	 */
	int value_;
	/**
	 * Position at table of the player who has the blind.
	 */
	int playerPosition_;
};

/**
 * Class representing object of the poker table. There can be multiple poker tables in the game.
 * All the game logic is done here.
 */
class Table {
public:
	typedef boost::unique_lock<boost::shared_mutex> uniqueLock;
	typedef boost::shared_lock<boost::shared_mutex> sharedLock;
	typedef unsigned long long gameStateId_t;
	typedef boost::shared_ptr<Player> PlayerPtr;
	typedef std::deque<PlayerPtr>::iterator PlayerIterator;

	/**
	 * Class constructor.
	 * @param	tableId		Id of this table
	 * @param	minPlayersNum	minimum number of players to start the game
	 * @param	maxPlayerNum	maximum number of players at this table.
	 */
	Table(int tableId, int minPlayersNum, int maxPlayersNum);
	/**
	 * Class virtual destructor.
	 */
	virtual ~Table(){};
	/**
	 * Public member function.
	 * Executes player's action. When user send request to the server this function checks if it's his turn
	 * to make o move. If it's his turn function makes hcanges to the table according to the provided
	 * arguments.
	 * @param	playerId	ID of the player who made a move
	 * @param	playerAction	number representing action made by player
	 * @param	bid			money that player put to the pot.
	 */
	bool executePlayerAction(int playerId, int playerAction, unsigned long bid);
	/**
	 * Public member function.
	 * Returns table state as JSON string.
	 * @param	playerId	ID of the player who requested table state information.
	 * @return	table state as JSON String.
	 */
	std::string getTableStatus(int playerId);
	/**
	 * Public member function.
	 * Adds player to the table. Player has to exist in the game to be added to the table.
	 * @param	player	Pointer to the Player object, representing player who will be added to the table.
	 * @return	true - operation was successful, false - player was not added
	 */
	bool addPlayerToTable(PlayerPtr player);
	/**
	 * Public member function.
	 * Removes player from the table.
	 * @param	playerId	ID of the player who will be removed from the table.
	 * @return	true - operation was successful, false - player was not removed or was not assigned to this table.
	 */
	bool removePlayerFromTable(int playerId);
	/**
	 * Public member function.
	 * Check if particular player is already assigned to this table.
	 * @param	playerId	Id of the player to find.
	 * @return	true - player is already assigned to this table, false - player is not assigned to this table.
	 */
	bool isThere(int playerId);
	/**
	 * Public member function.
	 * Returns the number of players at the table.
	 * @return		number of players at the table
	 */
	int countPlayers();
	/**
	 * Returns current game state id - the id created during last game modification.
	 * @return		current game state id.
	 */
	gameStateId_t getLastModified();
	/**
	 * Public member function.
	 * Removes all inactive players from the table.
	 * @param	seconds		afters this time inactive players will be removed
	 */
	void removeAllIdleFor(int seconds);
	/**
	 * Public member function.
	 * Returns pointer to the object of a player with provided ID.
	 * @param	playerId	Id of the player we are looking for
	 * @return	pointer to the player object
	 */
	PlayerPtr getPlayer(int playerId);
	/**
	 * Public member function.
	 * Accessor reutrning currentPlayerId_ member variable.
	 * @return	currentPlayerId_ variable
	 */
	int getCurrentPlayerId(){return currentPlayerId_;	}

private:

	/**
	 * Private member function.
	 * Saves info about players' accounts to the database. It's called at the beginning of each hand.
	 */
	void saveStateToDatabase();
	/**
	 * Private member function.
	 * Starts new poker hand. Resets variables changed during previous round. Assigns the button, small blind
	 * and big blind. Shuffles cards and gives 2 cards to each player. Sets the current player.
	 * It's automatically called when the game finishes.
	 */
	void newHand();
	/**
	 * Private member function.
	 * Checks if all the best for this round has been finished.
	 * Bets are finished when 2 conditions are met:
	 * 1. all the players who didn't flod made their moves
	 * 2. all the players put the same amount of money to the pot
	 * @return	true if bets are finished, false otherwise
	 */
	bool betsFinished();
	/**
	 * Private member function.
	 * Subtracts money from player's money and adds it to the pot.
	 * It's called whenever player bets.
	 * @param	player	pointer to the player who bets
	 * @param	money	amount of money player bets
	 */
	void addToPot(PlayerPtr player, int money);
	/**
	 * Private member function.
	 * Finds a random free position at the table for the new player.
	 * @return	number representing position at the table assigned to the player
	 */
	int randomFreeSpot();
	/**
	 * Private member function.
	 * Returns pointer to the object of a player sitting at particular position at the table.
	 * @param	playerPosition	Position of the player we are looking for
	 * @return	pointer to the player object
	 */
	PlayerPtr getPlayerByPos(int playerPosition);
	/**
	 * Private member function.
	 * Returns pointer to the object of a player sitting on the right of the player at position
	 * provided as argument.
	 * @param	prevPlayerPos	Position of the player current player
	 * @return	pointer to the player who sits on the right of current player
	 */
	PlayerPtr getNextPlayer(int prevPlayerPos);
	/**
	 * Private member function.
	 * Returns iterator to the object of a player with particular ID.
	 * Function returns only player who is still in the game.
	 * @param	playerId	ID of the player we are looking for
	 * @return	iterator to the player object in players_ deque.
	 */
	PlayerIterator getPlayerIter(int playerId);
	/**
	 * Private member function.
	 * Finds the position of the player sitting on the right of current player. Position of the current
	 * player is provided as function argument. Function returns only position of the player who is still in the game.
	 * @param	prevPlayerPos	position of the previous player
	 * @return	position of the player on the right to previous player
	 */
	int getNextPlayerPos(int prevPlayerPos);
	/**
	 * Private member function.
	 * Helper function sorting players assigned to the table by position.
	 * Players are sorted every time new player joins the table.
	 */
	void sortByPosition();
	/*
	 * Evaluating best hand. Function finds the best possible poker hand from all
	 * 7 available cards (5 common + 2 player's cards). The best cards combination is then copied
	 * to 'bestHand' vector. The integer representing best player's figure is returned.
	 * param	allCards	all 7 cards that can be used when looking for best hand
	 * param	bestHand	5 cards representing best player's hand
	 * return				integer representing user's best figure (9 - royal flush, 8- four, 7 - full etc)
	 */
	int evaluateHand(std::vector<Card> allCards, std::vector<Card> &bestHand);
	/**
	 * Private member function.
	 * Increments table state id. Should called every time table state is changed.
	 */
	void markModified();
	/**
	 * Private member function.
	 * Checks cards of all the players who didn't fold. It's called at the end of the hand.
	 * @return		ID of the player who won the hand.
	 */
	int checkCards();
	/**
	 * Private member function.
	 * It's a helper function the is used by checkCards function. When the best possible hand for a player is found
	 * we have to make sure that his other cards are the highest possible cards. This function finds highest cards
	 * in within the cards that are left on table and in player's hand and add them to bestHand vector.
	 * @param	allCards	all the cards player can use for evaluating hand (5 cards on table + 2 player's cards)
	 * @param	bextHand	best 5  cards are added to this vector
	 * @param	cardsNum	number of cards we have to add to bestHand to fill it with 5 cards
	 */
	void fillHand(std::vector<Card> &allCards, std::vector<Card> &bestHand, int cardsNum);
	/**
	 * Private member function.
	 * Helper function used while checking cards. Compares two identical hands (with the same figure)
	 * param	p1	pointer the the first player who's cards will be compared
	 * param	p2	pointer the the second player who's cards will be compared
	 * return	-1 if p1's cards are better, 0 - if p2's cards are better
	 */
	int compareTwoHands(PlayerPtr p1, PlayerPtr p2);
	/**
	 * Private member variable
	 * Number of players at the table. Container contains shared pointer to the players objects.
	 */
	std::deque<PlayerPtr> players_;
	/**
	 * Private member variable.
	 * Number representing changes in the game state. Number is increased every time something changes
	 * on the table. It is used to find out if there is new data for players.
	 */
	gameStateId_t gameStateId_;
	/**
	 * Private member variable.
	 * Mutex used to lock access to the table while user's thread is performaing operations on data.
	 */
	boost::shared_mutex tableAccess_;
	/**
	 * Private member variable.
	 * Maximum number of the players that can join the table. This value is passed as the constructor
	 * argument.
	 */
	int maxPlayersNum_;
	/**
	 * Private member variable.
	 * Minimum number of player need to start the game. Game starts automatically when this number of players
	 * join the table. It is set as a constructor's parameter. Until this number of players join the table
	 * state of the table is set to waiting.
	 */
	int minPlayersNum_;
	/**
	 * Private member variable.
	 * Cards that are taken from the deck and put on the table during each round.
	 * After all round we have 5 cards on the table.
	 * Variable is cleared at the beginning of each hand.
	 */
	std::vector<Card> cards_;
	/**
	 * Private member variable.
	 * Object representing the deck of cards. It is shuffled at the beginning of each hand.
	 */
	CardDeck deck_;
	/**
	 * Private member variable.
	 * Small blind object. Contains information about blind value and position of the player
	 * who has the small blind in current round.
	 */
	Blind smallBlind_;
	/**
	 * Private member variable.
	 * Big blind object. Contains information about blind value and position of the player
	 * who has the big blind in current round.
	 */
	Blind bigBlind_;
	/**
	 * Private member variable.
	 * Position at table of the player who is a dealer in current hand.
	 */
	int theButton_;
	/**
	 * Private member variable.
	 * The minimum bet value at the beginning of the game.
	 * The small blind value is set to this value
	 */
	int minBet_;
	/**
	 * Private member variable.
	 * Represents the state of the table.
	 * Can have values 0 - waiting for players, 1 - playing, 2 - hand finished.
	 */
	int tableState_;
	/**
	 * Private member variable.
	 * Represents the ID of current player.
	 * Current player is the player that is expected to make a move now.
	 */
	int currentPlayerId_;
	/**
	 * Private member variable.
	 * Represents the position of current player at the table.
	 * Current player is the player that is expected to make a move now.
	 */
	int currentPlayerPosition_;
	/**
	 * Private member variable.
	 * Number of the current hand. Each game is called a hand.
	 */
	int handNumber_;
	/**
	 * Private member variable.
	 * Number of current round (from 0 to 4).
	 * 0 - preflop, 1 - flop, 2- turn, 3 - river, 4 - showdown.
	 */
	int roundNumber_;
	/**
	 * Private member variable.
	 * Amount of money in the jackpot.
	 */
	int pot_;
	/**
	 * Private member variable.
	 * Id of the player that won the current hand.
	 */
	int handWinner_;
	/**
	 * Private member variable.
	 * Id of the current table.
	 */
	int tableId_;
	/**
	 * Private member variable.
	 * Amount of money in the particular round that defines the highest bet.
	 * All players have to put this amount of money in the pot to continue playing.
	 */
	int roundAmount_;
	/**
	 * Private static member array.
	 * Contains names of possible table positions.
	 * It's used to translate int variable tableState_ to name of the state.
	 */
	static const std::string tableStates[];
};


#endif /* TABLE_HPP_ */
