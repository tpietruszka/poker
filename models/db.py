# -*- coding: utf-8 -*-
import poker
import datetime
# # by default give a view/generic.extension to all actions from localhost
# # none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []

db = DAL('sqlite://storage.sqlite')

# db.define_table('users', 
#                Field('login', 'string', length=))

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

# # create all tables needed by auth if not custom tables
# auth.define_tables(username=False, signature=False)
# # after auth = Auth(db)

db.define_table(
    auth.settings.table_user_name,
        #część własna:
    #stan konta - ustalany na poczatku, nastepnie zmieniany tylko poprzez modul "poker" 
    Field('money', 'decimal(8,2)', default=10000, writable=False, label="Stan konta"),
     #nazwa uzytkownika
    Field('screen_name', length=20, required=True, unique=True, label="Pseudonim"),
    Field('email', length=128, default='', unique=True),  # required
    Field('password', 'password', length=512,  # required
          readable=False, label="Hasło"),
    Field('registration_key', length=512,  # required
          writable=False, readable=False, default=''),
    Field('reset_password_key', length=512,  # required
          writable=False, readable=False, default=''),
    Field('registration_id', length=512,  # required
          writable=False, readable=False, default=''), 
               
   
   
   
                )



# # do not forget validators
custom_auth_table = db[auth.settings.table_user_name]  # get the custom_auth_table
#custom_auth_table.password.requires = [IS_STRONG(), CRYPT()]
custom_auth_table.password.requires = [CRYPT()]
custom_auth_table.email.requires = [
  IS_EMAIL(error_message=auth.messages.invalid_email),
  IS_NOT_IN_DB(db, custom_auth_table.email)]

auth.settings.table_user = custom_auth_table  # tell auth to use custom_auth_table


auth.define_tables()
# # configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

# # configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True








#########################################################################
# # Define your tables below (or better in another model file) for example
# #
# # >>> db.define_table('mytable',Field('myfield','string'))
# #
# # Fields can be 'string','text','password','integer','double','boolean'
# #       'date','time','datetime','blob','upload', 'reference TABLENAME'
# # There is an implicit 'id integer autoincrement' field
# # Consult manual for more options, validators, etc.
# #
# # More API examples for controllers:
# #
# # >>> db.mytable.insert(myfield='value')
# # >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
# # >>> for row in rows: print row.id, row.myfield
#########################################################################
