# -*- coding: utf-8 -*-

@auth.requires_login()
def index():
    """
    Wypisuje listę istniejących stołów i liczby graczy przy nich
    """
    tables = poker.game.get_table_ids()
    tablePlayersCounts = {}
    for table in tables:
        tablePlayersCounts[table] = poker.game.count_table_players(table)
    return dict(tablePlayersCounts = tablePlayersCounts)
    
@auth.requires_login()
def create():
    """
    Tworzy nowy stół
    W wersji kolejnej należy dodać mechanizm obrony przed "spamem"
    """
    poker.game.create_table(3, 10)
    session.flash = "Dodano stół"
    redirect(URL('index'))

@auth.requires_login()
def join():
    """
    Dołączenie do gry; gra jest podtrzymywana przez 
    requesty AJAXowe, realizowane kontrolerze game.
    """
    if(len(request.args) != 1):
        session.flash = "Należy wybrać stół!"
        redirect(URL('index'))
    
    tableId = int(request.args[0])
    username = auth.user.screen_name
    userId = auth.user.id
    playerPosition = -1
    try: 
        playerPosition = poker.game.get_player_position(userId)
    except Exception as e:
        try:
            added = poker.game.add_player(userId)
        except Exception as e:
#            response.flash += e.message
            added = 0
        try:
            joined = poker.game.join_table(tableId, userId)
        except Exception as e:
            response.flash += e.message
            joined = 0
            
    playerPosition = poker.game.get_player_position(userId)
    if(playerPosition < 0): # nie udało się dołączyć gracza
        session.flash += "Błąd połączenia, proszę spróbowac ponownie"
        redirect(URL('index'))

    return dict(locals())

@auth.requires_login()
def leave():
    poker.game.leave_table(auth.user.id)
    redirect(URL('index'))
     
    