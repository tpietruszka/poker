# -*- coding: utf-8 -*-
# Kontroler odpowiadający za przebieg gry (nie: dołączanie). 
# Działa przez AJAX
import datetime
import poker
import time
from gluon.serializers import json
import gluon.contrib.simplejson
   
@auth.requires_login()
def get_state():
    session.forget(response)
    wait_quant = 0.2  # jednostkowe oczekiwanie w aktywnym oczekiwaniu na zmianę [s]
    response_timeout = datetime.timedelta(seconds=20)  # maksymalne opóźnienie wysłania odpowiedzi
    zombie_timeout = 30 # opóźnienie, po którym użytkownik zostanie uznany za zombie
    

    userId = auth.user.id
    poker.game.mark_player_request(userId)
    #TODO: odkomentować - usuwać zombie
    poker.game.remove_all_idle_for(zombie_timeout)
    
    # id ostatniego stanu, jaki otrzymała przeglądarka
    browser_last = request.vars.last_modified; 
    if(browser_last == None):
        browser_last = 0;
    browser_last = int(browser_last)
    start_time = datetime.datetime.now();
    local_last = poker.game.get_last_modified(userId)
    while(local_last == browser_last
          and (datetime.datetime.now() - start_time < response_timeout)):
        time.sleep(wait_quant)  # jeśli nie ma nic do przesłania - czekaj
        local_last = poker.game.get_last_modified(userId)
        
    # ostatecznie nic się nie zmieniło - prześlij "changed"=false dla podtrzymanai połączenia
    if(local_last == browser_last):
        return XML(json(dict(changed=False)))
    
    
    #stan gry

    stateString = poker.game.get_game_status(auth.user.id)
    state = gluon.contrib.simplejson.loads(stateString)
    for key in state['players'].keys():
        id = state['players'][key]['nick'] # moduł C++ zwraca tylko ID - tłumaczenie na reprezentację przez "screen_name"
        state['players'][key]['nick'] = db.auth_user[id].screen_name 
    if(int(state['winner']) > -1):
        state['winner'] = db.auth_user[state['winner']].screen_name

    state['playersCount'] = poker.game.count_all_players()
   
    res = dict()
    res['changed']=True;
    res['lastModified'] = local_last
    res['gameState']=state
    
    return XML(json(res))
@auth.requires_login()
def action():
    """
    Wołana przez AJAX, realizuje działanie zlecone przez gracza
    """
    userId = auth.user.id
    actionString = request.vars.action
    bid = int(request.vars.bid)
    
    
    actionNamesToNumbers={'fold': 0, 'check': 1, 'call': 2} #mapowanie nazw akcji na liczby
    action = actionNamesToNumbers[actionString]
    try:
        res = poker.game.execute_player_action(userId, action, bid)
    except Exception as e:
        session.flash = e.message
        res = 'True'
    return XML(json(res))


